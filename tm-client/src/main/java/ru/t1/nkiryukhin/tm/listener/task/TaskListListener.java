package ru.t1.nkiryukhin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.request.TaskListRequest;
import ru.t1.nkiryukhin.tm.dto.response.TaskListResponse;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Display all tasks";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @NotNull final TaskListResponse response = taskEndpoint.listTask(request);
        @Nullable List<TaskDTO> tasks = response.getTasks();
        if (tasks == null) tasks = Collections.emptyList();
        renderTasks(tasks);
    }

}
