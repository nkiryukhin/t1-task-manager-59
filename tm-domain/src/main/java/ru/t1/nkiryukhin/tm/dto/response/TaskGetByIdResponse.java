package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskGetByIdResponse extends AbstractTaskResponse {

    public TaskGetByIdResponse(@Nullable TaskDTO task) {
        super(task);
    }

}