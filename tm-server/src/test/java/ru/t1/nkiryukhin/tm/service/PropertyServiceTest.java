package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.configuration.ServerConfiguration;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;


@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private static final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IPropertyService propertyService = context.getBean(IPropertyService.class);

    @Test
    public void getApplicationVersion() {
        Assert.assertNotNull(propertyService.getApplicationVersion());
    }

    @Test
    public void getAuthorName() {
        Assert.assertNotNull(propertyService.getAuthorName());
    }

    @Test
    public void getAuthorEmail() {
        Assert.assertNotNull(propertyService.getAuthorEmail());
    }

    @Test
    public void getDatabaseName() {
        Assert.assertNotNull(propertyService.getDatabaseName());
    }

    @Test
    public void getDatabaseUrl() {
        Assert.assertNotNull(propertyService.getDatabaseUrl());
    }

    @Test
    public void getDatabaseUser() {
        Assert.assertNotNull(propertyService.getDatabaseUser());
    }

    @Test
    public void getDatabasePassword() {
        Assert.assertNotNull(propertyService.getDatabasePassword());
    }

}

