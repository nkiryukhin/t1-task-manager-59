package ru.t1.nkiryukhin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    M add(@Nullable String userId, M model) throws AbstractFieldException, UserNotFoundException, AbstractException;

    void clear(@NotNull String userId) throws UserIdEmptyException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractFieldException;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws UserIdEmptyException;

    @Nullable
    List<M> findAll(@NotNull String userId) throws UserIdEmptyException;

    @Nullable
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws UserIdEmptyException;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws AbstractFieldException;

    int getSize(@NotNull String userId) throws UserIdEmptyException;

    void remove(@Nullable Collection<M> collection) throws AbstractException;

    void removeById(@Nullable String userId, @Nullable String id) throws AbstractFieldException;

    void removeOne(@Nullable M model);

}