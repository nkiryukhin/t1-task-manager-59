package ru.t1.nkiryukhin.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.nkiryukhin.tm.api.service.model.IUserOwnedService;
import ru.t1.nkiryukhin.tm.api.service.model.IUserService;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;
import ru.t1.nkiryukhin.tm.model.User;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository();

    @Autowired
    protected IUserService userService;

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) return null;
        User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        model.setUser(user);
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().clear(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return getRepository().findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(userId, id);
    }

    @Override
    public int getSize(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().getSize(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return;
        for (M model : models) {
            removeOne(model);
        }
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        removeOne(model);
    }

}