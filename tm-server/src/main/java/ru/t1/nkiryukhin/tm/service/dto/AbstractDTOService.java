package ru.t1.nkiryukhin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.repository.dto.IDTORepository;
import ru.t1.nkiryukhin.tm.api.service.dto.IDTOService;
import ru.t1.nkiryukhin.tm.dto.model.AbstractModelDTO;

@Service
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IDTOService<M> {

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) return;
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) return;
        getRepository().update(model);
    }

}
