package ru.t1.nkiryukhin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    void clear(@NotNull String userId);

    @Nullable
    List<Session> findAll(@NotNull String userId);

    @Nullable
    Session findOneById(@NotNull String id);

    int getSize();

}