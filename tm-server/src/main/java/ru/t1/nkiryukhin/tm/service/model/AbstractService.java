package ru.t1.nkiryukhin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.repository.model.IRepository;
import ru.t1.nkiryukhin.tm.api.service.model.IService;
import ru.t1.nkiryukhin.tm.model.AbstractModel;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected abstract IRepository<M> getRepository();

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) return;
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) return;
        getRepository().update(model);
    }

}
